FROM python:3.8
RUN mkdir /code
ENV FLASK_DEBUG=1
WORKDIR /code
COPY requirements.txt /code/
COPY calculator_api /code/
RUN pip install -r requirements.txt
ENTRYPOINT python -m flask run --host=0.0.0.0 --port=5000
