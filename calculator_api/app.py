import json
from flask import Flask
from flask_restx import Api, Resource, fields

app = Flask(__name__)
api = Api(app, version='1.0', title='Calculator API',
    description='A Calculator API',
)

ns = api.namespace('numbers', description='calculator numbers')
ns_op = api.namespace('operations', description='do operations')


calculator = api.model('Calculator', {
    'id': fields.Integer(readonly=True, description='The number unique identifier'),
    'number': fields.Float(required=True, description='The number')
})

result = api.model('Result', {
    'result': fields.Float(required=True, description='The number')
})

operation = api.model('Operation', {
    'id': fields.Integer(readonly=True, description='The id unique identifier'),
    'operation': fields.String(required=True, description='The operation')
})

class Result(object):
    def __init__(self):
        result = None

class Calculator(object):
    def __init__(self):
        self.counter = 0
        self.list_number = []

    def get(self, id):
        for number in self.list_number:
            if number['id'] == id:
                return number
        api.abort(404, "Number {} doesn't exist".format(id))

    def create(self, data):
        number = data
        number['id'] = self.counter = self.counter + 1
        self.list_number.append(number)
        return number

    def update(self, id, data):
        number = self.get(id)
        number.update(data)
        return number

    def delete(self, id):
        number = self.get(id)
        self.list_number.remove(number)

class Operation(object):
    def __init__(self):
        self.counter = 0
        self.list_operation = []

    def get(self, id):
        for operation in self.list_operation:
            if operation['id'] == id:
                return operation
        api.abort(404, "operation {} doesn't exist".format(id))

    def create(self, data):
        operation = data
        operation['id'] = self.counter = self.counter + 1
        self.list_operation.append(operation)
        return operation

    def update(self, id, number1, number2, data):
        operation = self.get(id)
        print(operation)
        x = lambda a, b, c: eval(a+c+b)
        return {'result': x(number1, number2, operation['operation'])}

    def delete(self, id):
        operation = self.get(id)
        self.list_operation.remove(operation)


CALCUL = Calculator()
CALCUL.create({'number': 5})
CALCUL.create({'number': 30})
CALCUL.create({'number': 650})

OPERATION = Operation()
OPERATION.create({'operation': '-'})
OPERATION.create({'operation': '+'})
OPERATION.create({'operation': '/'})
OPERATION.create({'operation': '*'})


@ns.route('/')
class NumberList(Resource):
    '''Shows a list of all numbers, and lets you POST to add new numbers'''
    @ns.doc('list_number')
    @ns.marshal_list_with(calculator)
    def get(self):
        '''List all numbers in our api'''
        return CALCUL.list_number

    @ns.doc('create_number')
    @ns.expect(calculator)
    @ns.marshal_with(calculator, code=201)
    def post(self):
        '''Create a new number'''
        return CALCUL.create(api.payload), 201


@ns.route('/<int:id>')
@ns.response(404, 'Number not found')
@ns.param('id', 'The number identifier')
class Number(Resource):
    '''Show a single Number item and lets you delete them'''
    @ns.doc('get_number')
    @ns.marshal_with(calculator)
    def get(self, id):
        '''Fetch a given resource'''
        return CALCUL.get(id)

    @ns.doc('delete_number')
    @ns.response(204, 'Number deleted')
    def delete(self, id):
        '''Delete a number given its identifier'''
        CALCUL.delete(id)
        return '', 204

    @ns.expect(calculator)
    @ns.marshal_with(calculator)
    def put(self, id):
        '''Update a number given its identifier'''
        return CALCUL.update(id, api.payload)


# Operation part 
@ns_op.route('/')
class OperationList(Resource):
    '''Shows a list of all operation available, and lets you POST to add new operation'''
    @ns_op.doc('list_operation')
    @ns_op.marshal_list_with(operation)
    def get(self):
        '''List all operations in our api'''
        return OPERATION.list_operation

    @ns.doc('create_operation')
    @ns.expect(operation)
    @ns.marshal_with(operation, code=201)
    def post(self):
        '''Create a new operation'''
        return OPERATION.create(api.payload), 201

@ns_op.route('/<int:id>')
@ns_op.response(404, 'operation not found')
@ns_op.param('id', 'The operation identifier')
class OperationFind(Resource):
    '''Find an operation with the identifier and number identifier.'''
    @ns_op.doc('get_operation')
    @ns_op.marshal_with(operation)
    def get(self, id):
        '''Fetch a given resource'''
        return OPERATION.get(id)

    @ns.doc('delete_operation')
    @ns.response(204, 'operation deleted')
    def delete(self, id):
        '''Delete an operation given its identifier'''
        OPERATION.delete(id)
        return 'operation have been deleted', 204

@ns_op.route('/<int:id>/<int:number1>/<int:number2>')
@ns_op.response(404, 'operation not found')
@ns_op.param('id', 'The operation identifier')
class OperationAction(Resource):
    '''Do an operation with the identifier and number identifier.'''
    @ns_op.doc('do_operation')
    @ns_op.expect(operation)
    @ns_op.marshal_with(result)
    def put(self, id, number1, number2):
        '''Update a number by doing operation'''
        num1 = CALCUL.get(number1)['number']
        num2 = CALCUL.get(number2)['number']
        print(num1, num2)
        return OPERATION.update(id, str(num1), str(num2), api.payload)

if __name__ == '__main__':
    app.run(debug=True)