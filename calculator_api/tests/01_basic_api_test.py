
import json
import requests

headers = {
        "Accept": "*/*",
        "Accept-Encoding": "gzip, deflate",
        "Content-Length": "39",
        "Content-Type": "application/json",
        "Host": "httpbin.org",
        "User-Agent": "Python Requests"
    }

def test_get_list_operations_check_status_code_equals_200():
     response = requests.get("http://localhost:5000/operations")
     assert response.status_code == 200

def test_get_operation_by_id_check_status_code_equals_200():
     response = requests.get("http://localhost:5000/operations/1")
     response_body = response.json()
     assert response_body["operation"] == "-"
     assert response.status_code == 200

def test_get_list_numbers_check_status_code_equals_200():
     response = requests.get("http://localhost:5000/numbers")
     assert response.status_code == 200

def test_get_number_by_id_check_status_code_equals_200():
     response = requests.get("http://localhost:5000/numbers/1")
     assert response.status_code == 200

def test_get_number_by_id_check_status_code_equals_404():
     response = requests.get("http://localhost:5000/numbers/1000000000000")
     assert response.status_code == 404

def test_post_number_check_status_code_equals_201():
     data = json.dumps({"number": 340})
     response = requests.post("http://localhost:5000/numbers/", data=data, headers=headers)
     assert response.status_code == 201

def test_delete_number_check_status_code_equals_204():
     response = requests.delete("http://localhost:5000/numbers/4")
     assert response.status_code == 204

def test_post_operation_check_status_code_equals_201():
     data = json.dumps({"operation": '/'})
     response = requests.post("http://localhost:5000/operations/", data=data, headers=headers)
     assert response.status_code == 201

def test_delete_operation_check_status_code_equals_204():
     response = requests.delete("http://localhost:5000/operations/4")
     assert response.status_code == 204

def test_do_an_operation_check_status_code_equals_200():
     response = requests.put("http://localhost:5000/operations/2/1/2")
     assert response.status_code == 200